import React from 'react';
import { ProductCard } from '../components';

export default {
    title: "Product Card",
    component: ProductCard,
    argTypes: {
        badge: {
            options: ["bestseller" ,"sale", 'sold out', "in stock", "none"],
            control: {type: "select"}
        },
        shadow: {
            options: [true, false],
            control: {type: "radio"}
        },
        productName: {
            control: {type: "text"}
        },
        price: {
            control: {type: "text"}
        },
        border: {
            options: [true, false],
            control: {type: "radio"}
        },
        previousPrice: {
            options: [true, false],
            control: {type: "radio"}
        },
        previousPriceLabel: {
            control: {type: "text"}
        },
    }
}

export const BestSeller = {
    args: {    
        badge: "bestseller",
        shadow: true,
        productName: "Nike Air Huarache",
        price: "129.99",
        border: false,
        previousPrice: false,
        previousPriceLabel: "0.00"
    }
}
export const Sale = () => <ProductCard productName="Nike Air Huarache" badge="sale" price="120.00"/>