import styled, { css } from 'styled-components'
import '../../global.css'

export const StyledProductCard = styled.div <{border: Boolean, shadow: Boolean}>`
    width: 225px;
    height: 320px;
    border-radius: 10px;
    border: ${(props) => props.border ? '1px #4692FF solid' : 'none'};
    background-color: #fff;
    padding: 18px 18px 24px 18px;
    display: flex;
    flex-direction: column;
    box-shadow: ${(props) => props.shadow ? 'rgba(0, 0, 0, 0.24) 0px 3px 8px' : 'none' };
`
export const StyledBadge = styled.div <{badge: String}>`
    width: 69px;
    height: 21px;
    background-color: ${ (props) => props.badge === 'bestseller' ? '#AD90E4' : props.badge === 'sale' ? 'red' : props.badge === 'sold out' ? 'grey': 'green'};
    border-radius: 4px;
    display: ${ (props) => props.badge === 'none'? 'none' : 'flex' };
    justify-content: center;
    align-items: center;
`
export const StyledBadgeContent = styled.span `
    color: #fff;
    font-size: 10px;
    font-family: FuturaBold;
    text-transform: capitalize;
`
export const StyledProductImages = styled.div `
    height: 205px;
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-end;
`
export const StyledProductDetails = styled.div `
    width: 100%;
    height: calc(100% - 205px - 40px);
    padding-top: 24px;
    display: flex;
`
export const StyledProductDescription = styled.div `
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    width: 80%;
`
export const StyledSpanText = styled.span `
    font-family: FuturaBold;
    font-size: 14px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: -0.74px;
    color: #000;
`
export const StyledSpanPrice = styled.span `
    width: 40px;
    height: 16px;
    font-family: FuturaBold;
    font-size: 10px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: -0.57px;
    color: #000;
`
export const StyledCartIcon = styled.img `
    width: 33px;
    height: 33px;
    object-fit: contain;
    width: 20%;
`
export const StyledDivider = styled.div `
    border-top: solid;
    width: 100%;
    border-color: #4491FE;
    border-width: 2px;
`
export const StyledMainImage = styled.img `
    width: 100%;
    height: 87px;
    object-fit: contain;
`
export const StyledMultipleImages = styled.img `
    width: 100%;
    height: 29px;
    object-fit: contain;
    margin-top: 20px;
    margin-bottom: 20px;
`
export const StyledPicesContainer = styled.div `
    display: flex;
    flex-direction: row;
    width: 100%;
    margin-top: 5px;
`
export const StyledSpanPreviousPrice = styled(StyledSpanPrice) <{previousPrice: Boolean}>`
    display: ${(props) => props.previousPrice ? "flex" : "none"};
    text-decoration: line-through;
    color: grey;
    margin-left: 15px;
`