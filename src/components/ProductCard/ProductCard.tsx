import React, { FC } from "react";
import cartIcon from '../../assets/cart-button3x.png'
import mainImage from '../../assets/shoes-main3x.png'
import multipleImage from '../../assets/shoes-small3x.png'
import { 
    StyledProductCard,
    StyledBadge,
    StyledBadgeContent,
    StyledProductImages,
    StyledMainImage,
    StyledMultipleImages,
    StyledDivider,
    StyledProductDetails,
    StyledProductDescription,
    StyledSpanText,
    StyledSpanPrice,
    StyledCartIcon,
    StyledPicesContainer,
    StyledSpanPreviousPrice
} from './ProductCard.styled'

export interface Props {
    badge: String,
    shadow?: Boolean,
    productName: String,
    price: String,
    border?: Boolean,
    previousPrice?: Boolean,
    previousPriceLabel?: String
}

const ProductCard: FC<Props> = (
    {
        badge="bestseller", 
        shadow=false,
        productName="Product Name",
        price="0.00",
        border= false,
        previousPrice= false,
        previousPriceLabel='0.00'
    }) => {
    return (
        <StyledProductCard shadow={shadow} border={border} >
            <StyledBadge badge={badge}>
                <StyledBadgeContent>{badge}</StyledBadgeContent>
            </StyledBadge>
            <StyledProductImages>
                <StyledMainImage src={mainImage} alt="main-product-img"></StyledMainImage>
                <StyledMultipleImages src={multipleImage} alt="multiple-product-img"></StyledMultipleImages>
            </StyledProductImages>
            <StyledDivider />
            <StyledProductDetails>
                <StyledProductDescription>
                    <StyledSpanText >{productName}</StyledSpanText>
                    <StyledPicesContainer>
                        <StyledSpanPrice>${price}</StyledSpanPrice>
                        <StyledSpanPreviousPrice previousPrice={previousPrice}>${previousPriceLabel}</StyledSpanPreviousPrice>
                    </StyledPicesContainer>
                </StyledProductDescription>
                <StyledCartIcon src={cartIcon} alt="logo"></StyledCartIcon>
            </StyledProductDetails>
        </StyledProductCard>
    )
}

export default ProductCard